module top (
    input i_Clk,

    input i_Switch_1,
    // input i_Switch_2,
    // input i_Switch_3,
    // input i_Switch_4,

    // output o_LED_1,
    // output o_LED_2,
    // output o_LED_3,
    // output o_LED_4,

    output o_VGA_HSync,
    output o_VGA_VSync,
    output o_VGA_Red_0,
    output o_VGA_Red_1,
    output o_VGA_Red_2,
    output o_VGA_Grn_0,
    output o_VGA_Grn_1,
    output o_VGA_Grn_2,
    output o_VGA_Blu_0,
    output o_VGA_Blu_1,
    output o_VGA_Blu_2
);
    reg rst = 1;
    always @(posedge i_Clk) begin
        rst <= i_Switch_1;
    end

    wire[2:0] red;
    wire[2:0] green;
    wire[2:0] blue;

    assign o_VGA_Red_0 = red[0];
    assign o_VGA_Red_1 = red[1];
    assign o_VGA_Red_2 = red[2];

    assign o_VGA_Grn_0 = green[0];
    assign o_VGA_Grn_1 = green[1];
    assign o_VGA_Grn_2 = green[2];

    assign o_VGA_Blu_0 = blue[0];
    assign o_VGA_Blu_1 = blue[1];
    assign o_VGA_Blu_2 = blue[2];

    e_proj_main_main main
        ( .clk_i(i_Clk)
        , .rst_i(rst)
        , .in_unsync_i(i_Switch_1)
        // , .output__({ o_LED_1, o_LED_2, o_LED_3, o_LED_4 })
        , .output__({ o_VGA_HSync, o_VGA_VSync, red, green, blue })
        // , .__output(o_LED_1)
        );
endmodule
